package com.zuitt.capstone.services;

import com.zuitt.capstone.models.Course;

public interface CourseService {
    void createCourse(String stringToken, Course course);
    Iterable<Course> getCourses();

}

